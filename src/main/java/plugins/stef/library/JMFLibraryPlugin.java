package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the JMF (Java Media Framework) library.
 * 
 * @author Stephane Dallongeville
 */
public class JMFLibraryPlugin extends Plugin implements PluginLibrary
{
    //
}
